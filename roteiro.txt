git help: Mostra vários comandos de forma resumida

git help <comando>: Mostra detalhes de um comando específico

git config --global user.name “Seu Nome”
git config --global user.email “seuemaill@gmail.com”   (Usar o mesmo email do gitlab)
 
Para o repositório atual: --local;
Para o Usuário: --global;
Para o Computador: --system

Opcional:
(primeiro testa code -v)
git config --global core.editor 'code --wait' -> Vscode para editor padrão (precisa do code no PATH)


	Se tudo der certo:
	git config --global -e -> Abre o arquivo de configuração no vscode

-------------------------------------------------------


git init: inicializa o repositório git no local desejado

git status: usado para verificar o estado do arquivo no momento 

git add arquivo/diretório: adiciona um arquivo ou um arquivo de um diretrório ao staging, usado frequentemente para atualizar arquivos que foram alterados
git add --all = git add . : adiciona de uma única vez todos os arquivos que foram alterados no projeto

git commit -m “Primeiro commit”: junto a atualização de um arquivo, cria um commit(um ponto na história do projeto) com a mensagem informando que aquele foi o primeiro commit, e informando também quem fez aquele commit e quando

-------------------------------------------------------

git log: mostra o histórico de alterações do projeto em ordem cronológica
git log arquivo : mostra as alterações feitas em um determinado arquivo
git reflog: resgistram quando o cume dos ramos e também outras referências foram atualizadas no repositório local 

-------------------------------------------------------

git show: mostra detalhes do commit atual 
git show <commit>: mostra detalhes do commit indicado 

-------------------------------------------------------

git diff : compara o commit que está na area de trabalho e o commit mais recente
git diff <commit1> <commit2>: compara dois commit desesajdos, mostrando as alterações 

-------------------------------------------------------

git reset --hard <commit> : desfaz todas as alterações de um commit desejado

-------------------------------------------------------

git branch: lista todas as branches locais
git branch -r : lista todas as branches remotas
git branch -a : lista todas as branches locais e remotas
git branch -d <branch_name>: remove uma branch local indicada
git branch -D <branch_name> : força a remoção de uma branch local indicada
git branch -m <nome_novo>: faz o mesmo que o comando abaixo, mas nesse caso estamos na branch a ser alterada
git branch -m <nome_antigo> <nome_novo>: muda o nome da branch indicada para o nome novo

-------------------------------------------------------

git checkout <branch_name> : nos leva até a branch indicada
git checkout -b <branch_name> : cria a branch e nos direciona a ela ao mesmo tempo

-------------------------------------------------------

git merge <branch_name>: incorpora as mudanças da branch_name na branch corrente 

-------------------------------------------------------

git clone : clona o repositório para a pasta corrente 
git pull: atualiza a versão local de um repositório a partir de uma versão remota 
git push: envia o conteúdo do repositório local para um repositório remoto

-------------------------------------------------------

git remote -v: lista os servidores remotos associados a URL que estão sendo usados pelo repositório 
git remote add origin <url>: compartilha o repositório local com o serviço de controle baseado em nuvem 
git remote <url> origin: troca a url do servidor origin para uma nova url

--------------------------------------------------------

Documentação do git:
https://git-scm.com/doc

Playlist GIT:
https://www.youtube.com/playlist?list=PLucm8g_ezqNq0dOgug6paAkH0AQSJPlIe

Vídeo sobre Git: 
https://www.youtube.com/watch?v=kB5e-gTAl_s


